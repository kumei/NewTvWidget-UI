package com.open.ui.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;

import com.open.ui.R;

/**
 * 参考 QMUIRoundButtonDrawable
 */
public class TVUIRoundDrawable extends GradientDrawable {

    private int mStrokeWidth = 0;
    private ColorStateList mStrokeColors;

    public static TVUIRoundDrawable build(Context context, AttributeSet attrs, int defStyleAttr) {
        final TVUIRoundDrawable uiRoundDrawable = new TVUIRoundDrawable();

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.UIRoundButton, defStyleAttr, 0);
        ColorStateList bgColor = ta.getColorStateList(R.styleable.UIRoundButton_tvui_backgroundColor); // 背景颜色
        ColorStateList borderColor = ta.getColorStateList(R.styleable.UIRoundButton_tvui_borderColor); // 边框颜色
        int borderWidth = ta.getDimensionPixelSize(R.styleable.UIRoundButton_tvui_borderWidth, 0); // 边框宽度
        int mRadius = ta.getDimensionPixelSize(R.styleable.UIRoundButton_tvui_radius, 0);
        int mRadiusTopLeft = ta.getDimensionPixelSize(R.styleable.UIRoundButton_tvui_radiusTopLeft, 0);
        int mRadiusTopRight = ta.getDimensionPixelSize(R.styleable.UIRoundButton_tvui_radiusTopRight, 0);
        int mRadiusBottomLeft = ta.getDimensionPixelSize(R.styleable.UIRoundButton_tvui_radiusBottomLeft, 0);
        int mRadiusBottomRight = ta.getDimensionPixelSize(R.styleable.UIRoundButton_tvui_radiusBottomRight, 0);
        ta.recycle();
        /* 设置 边框宽度与颜色 */
        uiRoundDrawable.setStrokeData(borderWidth, borderColor);
        /* 设置背景颜色 */
        uiRoundDrawable.setBgData(bgColor);
        /* 设置圆角角度 */
        if (mRadiusTopLeft > 0 && mRadiusTopRight > 0 && mRadiusBottomLeft > 0 && mRadiusBottomRight > 0) {
            float[] radii = new float[]{
                    mRadiusTopLeft, mRadiusTopLeft,
                    mRadiusTopRight, mRadiusTopRight,
                    mRadiusBottomRight, mRadiusBottomRight,
                    mRadiusBottomLeft, mRadiusBottomLeft
            };
            uiRoundDrawable.setCornerRadii(radii);
        } else {
            uiRoundDrawable.setCornerRadius(mRadius);
        }

        // eg.
        // ColorStateList borderColor = context.getResources().getColorStateList(R.color.s_app_color_blue_3);
        // ColorStateList bgColor = context.getResources().getColorStateList(R.color.s_app_color_blue_to_red);
        // uiRoundDrawable.setStrokeData(2, borderColor);

        return uiRoundDrawable;
    }

    TVUIRoundDrawable() {
        // 设置四边圆角
//        int mRadiusTopLeft = 10;
//        int mRadiusTopRight = 10;
//        int mRadiusBottomLeft = 10;
//        int mRadiusBottomRight = 10;
//        float[] radii = new float[]{
//                mRadiusTopLeft, mRadiusTopLeft,
//                mRadiusTopRight, mRadiusTopRight,
//                mRadiusBottomRight, mRadiusBottomRight,
//                mRadiusBottomLeft, mRadiusBottomLeft
//        };
//        setCornerRadii(radii);
//        setCornerRadius(10);
        // 边框样式
        // setStroke(2, Color.BLACK);
    }

    ColorStateList mFillColors;

    public void setBgData(ColorStateList colors) {
        if (hasNativeStateListAPI()) {
            super.setColor(colors);
        } else {
            mFillColors = colors;
            final int currentColor;
            if (colors == null) {
                currentColor = Color.TRANSPARENT;
            } else {
                currentColor = colors.getColorForState(getState(), 0);
            }
            setColor(currentColor);
        }
    }

    /**
     * 设置按钮的描边粗细和颜色
     */
    public void setStrokeData(int width, ColorStateList colors) {
        if (hasNativeStateListAPI()) {
            super.setStroke(width, colors);
        } else {
            mStrokeWidth = width;
            mStrokeColors = colors;
            final int currentColor;
            if (colors == null) {
                currentColor = Color.TRANSPARENT;
            } else {
                currentColor = colors.getColorForState(getState(), 0);
            }
            setStroke(width, currentColor);
        }
    }

    private boolean hasNativeStateListAPI() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    @Override
    protected boolean onStateChange(int[] stateSet) {
        boolean superResult = super.onStateChange(stateSet);
        // 边框样式状态获取
        if (mStrokeColors != null) {
            int color = mStrokeColors.getColorForState(stateSet, 0);
            setStroke(mStrokeWidth, color);
            superResult = true;
        }
        return superResult;
    }

    @Override
    public boolean isStateful() {
        return (null != mStrokeColors && mStrokeColors.isStateful())
                || super.isStateful();
    }

    @Override
    protected void onBoundsChange(Rect r) {
        super.onBoundsChange(r);
//        setCornerRadius(Math.min(r.width(), r.height()) / 2);
    }

}
