package com.open.ui.widget;

import java.io.FileNotFoundException;

/**
 * Created by XGIMI on 2017/8/9.
 */

public interface IVideoPlayView {

    void setVideo(String path) throws FileNotFoundException;

    void openPlayer();

    void setVideoListener(TVUIVideoPlayerView.OnVideoPlayerListener listener);

    void play();

    void pause();

    void stop();

    void syncStop();

    int playOrPause();

    void seekTo(int position);

    int getPlayerState();

    void addSubtitle(String path);

}
