package com.open.ui.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Space;
import android.widget.TextView;

import androidx.collection.SimpleArrayMap;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.open.ui.R;
import com.open.ui.skin.ISkinLayout;
import com.open.ui.skin.SkinManager;
import com.open.ui.widget.itemview.TVUICommonItemViewBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * TVUICommonItemView
 * <ul>
 * <li>通过 {@link #setText(CharSequence)} (CharSequence)} 设置文本</li>
 * <li>通过 {@link #setDetailText(CharSequence)} 设置详情文本,
 * 也可以在 xml 中使用 {@link R.styleable#TVUICommonItemView_tvui_commonItem_detailText} 设置。</li>
 * <li>通过 {@link #setItemType(int)} (int)} 设置右侧 View 的类型,
 * 也可以在 xml 中使用 {@link R.styleable#TVUICommonItemView_tvui_commonItem_type} 设置。</li>
 * </ul>
 *
 * <pre>
 * [*] 进度条使用DEMO
 *
 * [*] CheckBox 使用DEMO
 *     TVUICommonItemView chexkBoxItemView ... ...
 *     正常使用
 *     chexkBoxItemView.setChecked(true);
 *     文本切换状态
 *     chexkBoxItemView.setCheckText("自动/手动");
 *     回调处理
 *     chexkBoxItemView.setOnItemCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
 *        {@literal @}Override
 *         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
 *             // 自己处理逻辑，去设置相关的文本
 *         }
 *     });
 * [*] 双箭头使用DEMO
 *     # XML属性
 *     tv:tvui_commonItem_str_array="@array/items" // 设置数组字符串
 *     # 代码方式
 *     TVUICommonItemView doubleArrorItemView ... ...
 *     // 设置数组字符串
 *     doubleArrorItemView.setDoubleArrowArray(new ArrayList<String>() {
 *         {
 *             add("测试1");
 *             add("测试2");
 *             add("测试3");
 *             add("测试4");
 *         }
 *     });
 *     // 设置显示第几个，不设置的话 setDoubleArrowArray 默认使用当前的index
 *     doubleArrorItemView.setDoubleArrowIndex(0);
 *     doubleArrorItemView.setOnDoubleArrowViewListener(new TVUICommonItemView.OnDoubleArrowViewListener() {
 *        {@literal @}Override
 *         public void onDoubleArrowSwitch(TVUICommonItemView itemView, int index) {
 *             // 这里处理自己的相关业务逻辑
 *         }
 *     });
 * </pre>
 */
public class TVUICommonItemView extends ConstraintLayout implements ISkinLayout {

    public final static int ITEM_TYPE_NULL = -250;
    /**
     * 没有任何东西，默认类型(文本/图标)
     */
    public final static int ITEM_TYPE_NONE = 0;
    /**
     * 右侧显示一个箭头
     */
    public final static int ITEM_TYPE_ARROW = 1;
    /**
     * 右侧显示一个开关
     */
    public final static int ITEM_TYPE_SWITCH = 2;
    /**
     * 右侧显示一个单选
     */
    public final static int ITEM_TYPE_RADIO = 3;
    /**
     * 左右侧显示2个箭头
     */
    public final static int ITEM_TYPE_DOUBLE_ARROW = 4;
    /**
     * 进度条
     */
    public final static int ITEM_TYPE_PROGRESS = 5;
    /**
     * 加载提示
     */
    public final static int ITEM_TYPE_LOADING = 6;
    /**
     * EditText 输入类型
     */
    public final static int ITEM_TYPE_EDITTEXT = 7;
    /**
     * EditText 输入类型
     */
    public final static int ITEM_TYPE_SECOND_TITLE = 9;
    /**
     * 带文本详情的类型
     */
    public final static int ITEM_TYPE_TEXT = 10;

    /**
     * WIFI类型
     */
    public final static int ITEM_TYPE_WIFI = 25;

    /**
     * 自定义右侧显示的 View
     */
    public final static int ITEM_TYPE_CUSTOM = 520;

    protected int mItemType; // Item类型保存

    protected ViewGroup mTypeView; // 右侧容器，比如进度，开关，箭头，自定义等
    protected ImageView mImageView; // 左侧图标
    protected ImageView mLeftArrowIv; // 左边的箭头
    protected ImageView mRightArrowIv; // 右边的箭头
    protected CheckBox mSwitch; // 开关/单选
    protected SeekBar mProgressBar; // 进度条
    protected TextView mPbTextView; // 进度条左侧文字显示

    protected LinearLayout mTextContainer;
    protected TextView mTextView;
    protected Space mTextSpace;
    protected TextView mDetailTextView;

    protected ViewStub mLoadingViewStub; // 加载提示 ViewStub
    protected ProgressBar mLoadingView; // 加载提示 View

    protected Drawable mArrowDrawable;
    float mTypeMarginLeft; // 箭头/文本间距
    protected ColorStateList mArrowColorStateList;
    protected float mIconSize;
    protected float mIconMarginRight;
    protected  Drawable mIconDrawable;
    protected ColorStateList mIconColorStateList;
    protected int mSwitchDrawableRes;
    protected int mRadioDrawableRes;

    float mTextMaxWidth;
    float mTextWidth;
    /**
     * EditText相关属性:
     *  tv:digits="0123456789."
     *  tv:inputType="number|text"
     *  tv:maxLength="15"
     */
//    protected ColorStateList mDetailColor;
//    float mDetailSize;
    float mDetailMaxWidth;
    float mDetailWidth;
    protected EditText mRightInputEt;
    protected int mInputType;
    protected String mDigits;
    /**
     双箭头
     */
    int mStrArrayRes;

    public TVUICommonItemView(Context context) {
        this(context, null);
    }

    public TVUICommonItemView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.TVUICommonItemViewStyle);
    }

    public TVUICommonItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
//        LayoutInflater.from(context).inflate(R.layout.tvui_common_item, this, true);
        // 获取属性值
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TVUICommonItemView, defStyleAttr, 0);
//        ColorStateList textColor = array.getColorStateList(R.styleable.TVUICommonItemView_tvui_commonItem_titleColor);
//        mDetailColor = array.getColorStateList(R.styleable.TVUICommonItemView_tvui_commonItem_detailColor);
        String text = array.getString(R.styleable.TVUICommonItemView_tvui_commonItem_titleText);
//        float textSize = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_titleTextSize, 0);
        mTextMaxWidth = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_titleTextMaxWidth, 0);
        mTextWidth = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_titleTextWidth, 0);
        String detail = array.getString(R.styleable.TVUICommonItemView_tvui_commonItem_detailText);
//        mDetailSize = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_detailTextSize, 0);
        mDetailMaxWidth = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_detailTextMaxWidth, 0);
        mDetailWidth = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_detailTextWidth, 0);
//        float detailLineSpace = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_detailTextLineSpace, 8);
        int itemType = array.getInt(R.styleable.TVUICommonItemView_tvui_commonItem_type, ITEM_TYPE_NULL);
        mIconDrawable = array.getDrawable(R.styleable.TVUICommonItemView_tvui_commonItem_icon);
        mIconColorStateList = array.getColorStateList(R.styleable.TVUICommonItemView_tvui_commonItem_ImageTint);
        mIconSize = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_iconSize, 0);
        mIconMarginRight = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_iconMarginRight, 5);
        mTypeMarginLeft = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_typeMarginLeft, 5);
        mArrowDrawable = array.getDrawable(R.styleable.TVUICommonItemView_tvui_commonItem_arrowIcon);
        mArrowColorStateList = array.getColorStateList(R.styleable.TVUICommonItemView_tvui_commonItem_ArrowTint);
        // 进度条相关属性
        Drawable progressDrawable = array.getDrawable(R.styleable.TVUICommonItemView_tvui_commonItem_progressDrawable);
        Drawable progressThumb = array.getDrawable(R.styleable.TVUICommonItemView_tvui_commonItem_thumb);
        float progressWidth = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_progressWidth, 80);
        float progressHeight = array.getDimension(R.styleable.TVUICommonItemView_tvui_commonItem_progressHeight, 8);
        int progressMax = array.getInteger(R.styleable.TVUICommonItemView_tvui_commonItem_progressMax, 100);
        int progress = array.getInteger(R.styleable.TVUICommonItemView_tvui_commonItem_progress, 0);
        // 开关相关属性
//        mSwitchDrawable = array.getDrawable(R.styleable.TVUICommonItemView_tvui_commonItem_switchDrawable);
//        mRadioDrawable = array.getDrawable(R.styleable.TVUICommonItemView_tvui_commonItem_radioDrawable);
        boolean isChecked = array.getBoolean(R.styleable.TVUICommonItemView_tvui_commonItem_checked, false);
        mSwitchDrawableRes = array.getResourceId(R.styleable.TVUICommonItemView_tvui_commonItem_switchDrawable, 0);
        mRadioDrawableRes = array.getResourceId(R.styleable.TVUICommonItemView_tvui_commonItem_radioDrawable, 0);
        // EditText相关属性
        mInputType = array.getInteger(R.styleable.TVUICommonItemView_tvui_commonItem_inputType, InputType.TYPE_CLASS_TEXT);
        mDigits = array.getString(R.styleable.TVUICommonItemView_tvui_commonItem_digits);
        mMaxLen = array.getInteger(R.styleable.TVUICommonItemView_tvui_commonItem_maxLength, 0);
        // 双箭头的数组字符串
        mStrArrayRes = array.getResourceId(R.styleable.TVUICommonItemView_tvui_commonItem_str_array, -1);
        //
//        array.getResourceId(R.styleable.TVUICommonItemView_tvui_commonItem_icon, View.NO_ID);
        array.recycle();
//        // 初始化控件
//        new TVUICommonItemViewBuilder().create(-250, this);
//        mImageView = (ImageView) findViewById(R.id.common_item_iv);
////        mTextContainer = (LinearLayout) findViewById(R.id.common_item_text_container);
//        mTextView = (TextView) findViewById(R.id.common_item_tv);
////        mTextSpace = (Space) findViewById(R.id.common_item_space);
////        mDetailTextView = (TextView) findViewById(R.id.common_item_detail_tv);
//        mLeftArrowIv = (ImageView) findViewById(R.id.common_item_left_arrow_iv);
////        mProgressBar = findViewById(R.id.common_item_pb);
////        mPbTextView = findViewById(R.id.common_item_pb_tv);
////        mTypeView = findViewById(R.id.common_item_type_view);
////        mLoadingViewStub = findViewById(R.id.common_item_loading_tip);
////        mRightInputEt = findViewById(R.id.common_item_input_et);
//        // 设置图标信息
//        setImageDrawable(iconDrawable);
//        if (null != mImageView) {
////            ConstraintLayout.LayoutParams iconLayoutParams = (ConstraintLayout.LayoutParams) mImageView.getLayoutParams();
////            iconLayoutParams.rightMargin = (int) iconMarginRight; // 设置图标与文字之间的间距
//
//            if (mIconSize > 0) {
//                ConstraintLayout.LayoutParams iconLayoutParams = (ConstraintLayout.LayoutParams) mImageView.getLayoutParams();
//                iconLayoutParams.width = (int) mIconSize;
//                iconLayoutParams.height = (int) mIconSize;
////                mImageView.setLayoutParams(iconLayoutParams);
//            }
//        }
//
//        if (null != mTextView && null != mImageView) { // 设置图标与文字之间的间距
//            ConstraintLayout.LayoutParams textLayoutParams = (ConstraintLayout.LayoutParams) mTextView.getLayoutParams();
//            textLayoutParams.leftMargin = (int) iconMarginRight;
////            mImageView.setLayoutParams(iconLayoutParams);
//        }
//
//        if (null != mIconColorStateList) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                setImageTintListAndMode(mIconColorStateList, PorterDuff.Mode.SRC_IN);
//            }
//        }
//        // 设置文本信息
////        if (null != textColor) {
////            mTextView.setTextColor(textColor);
////        }
////        if (textSize > 0) {
////            mTextView.setTextSize(textSize);
////        }
//        if (textMaxWidth > 0) {
//            setTextMaxWidth((int) textMaxWidth);
//        }
//        if (textWidth > 0) {
//            setTextWidth((int) textWidth);
//        }
//        setText(text);
//        // 设置详情信息
////        if (null != mDetailColor) {
////            mDetailTextView.setTextColor(mDetailColor);
////            mPbTextView.setTextColor(mDetailColor);
////        }
////        if (mDetailSize > 0) {
////            mDetailTextView.setTextSize(mDetailSize);
////            mPbTextView.setTextSize(mDetailTextView.getTextSize());
////        }
//        if (mDetailMaxWidth > 0) {
//            setDetailMaxWidth((int) mDetailMaxWidth);
//        }
//        if (detailWidth > 0) {
//            setDetailTextWidth((int) detailWidth);
//        }
//        setDetailText(detail);
//        // 设置右侧信息
//        if (null != mTypeView) {
//            LinearLayout.LayoutParams typeLayoutParams = (LinearLayout.LayoutParams) mTypeView.getLayoutParams();
//            typeLayoutParams.leftMargin = (int) typeMarginLeft;
//        }
//        if (null != mProgressBar) {
//            LinearLayout.LayoutParams pbLayoutParams = (LinearLayout.LayoutParams) mProgressBar.getLayoutParams();
//            pbLayoutParams.leftMargin = (int) typeMarginLeft; // 设置左侧文本与进度条的间距
//            pbLayoutParams.rightMargin = (int) typeMarginLeft; // 设置右侧文本与进度条的间距
//            pbLayoutParams.width = (int) progressWidth;
//            pbLayoutParams.height = (int) progressHeight;
//            mProgressBar.setProgressDrawable(progressDrawable);
//            mProgressBar.setThumb(progressThumb);
//        }
//        if (itemType == ITEM_TYPE_PROGRESS) {
//            setProgressMaxValue(progressMax);
//            setProgress(progress);
//        }
//        //
//        setItemType(itemType);
//        setClickable(true);
//        setFocusable(true);
//        setFocusableInTouchMode(true);
//        setClipChildren(false);
//        setClipToPadding(false);
//        // 初始化一些空间类型的数据.
//        if (itemType == ITEM_TYPE_RADIO || itemType == ITEM_TYPE_SWITCH) {
//            setChecked(isChecked);
//        } else if (itemType == ITEM_TYPE_DOUBLE_ARROW) {
//            setDoubleArrowArray(mStrArrayRes);
//        }
//        if (iconDrawable == null) {
//            setImageViewVisibility(View.GONE);
//        }
//        setWillNotDraw(false);
//        //
////        setThemeAttr(SkinManager.getInstance(getContext()).theme, SkinManager.getInstance(getContext()).mResources);

        // 重构后的初始化属性
        setClickable(true);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setClipChildren(false);
        setClipToPadding(false);
        setWillNotDraw(false);
        //
        setItemType(itemType);
        //
        setText(text);
        setDetailText(detail);
    }

    public void setThemeAttr(Resources.Theme theme, Resources resources) {
        for (int i = 0; i < skinAttrs.size(); i++) {
            TypedValue value = new TypedValue();
            String key = skinAttrs.keyAt(i); // background
            Integer attr = skinAttrs.valueAt(i); // R.attr....
            if (attr != null) {
                if (SkinManager.BACKGROUND_TAG.equals(key)) {
                    if (theme.resolveAttribute(attr, value, true)) {
                        Drawable drawable = ResourcesCompat.getDrawable(resources, value.resourceId, theme);
                        setBackgroundDrawable(drawable);
                    }
                } else if (SkinManager.TEXT_COLOR_TAG.equals(key)) {
                    ColorStateList color = SkinManager.getAttrColorStateList(getContext(), theme, attr);
                    setTextColor(color);
                } else if (SkinManager.SECOND_TEXT_COLOR_TAG.equals(key)) {
                    ColorStateList color = SkinManager.getAttrColorStateList(getContext(), theme, attr);
                    setDetailTextColor(color);
                } else if (SkinManager.IMAGE_TINT_TAG.equals(key)) {
                    ColorStateList color = SkinManager.getAttrColorStateList(getContext(), theme, attr);
                    setImageTintListAndMode(color, PorterDuff.Mode.SRC_IN);
                } else if (SkinManager.ARROW_TINT_TAG.equals(key)) {
                    ColorStateList color = SkinManager.getAttrColorStateList(getContext(), theme, attr);
                    setArrowTintListAndMode(color, PorterDuff.Mode.SRC_IN);
                }
            }
        }
    }

    @Override
    public boolean performClick() {
        boolean b = super.performClick();
        // 选中状态切换
        toggle();
        // 处理其它界面取消选中的状态
        resetAllCheckState();
        // 单击回调
        if (mOnClickListener != null) {
            mOnClickListener.onClick(TVUICommonItemView.this);
        }
        if (mItemType == ITEM_TYPE_EDITTEXT) {
            mRightInputEt.requestFocusFromTouch();
        }
        return b;
    }

    /**
     * 设置进度条的值
     *
     * @param value int数值
     */
    public void setProgress(int value) {
        if (mProgressBar != null) {
            mProgressBar.setProgress(value);
        }
    }

    /**
     * 获取进度条当前的值.
     *
     * @return int数值
     */
    public int getProgress() {
        if (mProgressBar != null) {
            return mProgressBar.getProgress();
        }
        return 0;
    }

    /**
     * 设置进度条最小值显示
     *
     * @param value int数值
     */
    public void setProgressMinValue(int value) {
        if (mPbTextView != null && mProgressBar != null) {
            mPbTextView.setText(value + "");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mProgressBar.setMin(value);
            }
        }
    }

    /**
     * 设置进度条的最大值显示
     *
     * @param value int数值
     */
    public void setProgressMaxValue(int value) {
        if (mPbTextView != null && mProgressBar != null) {
            mDetailTextView.setText("" + value);
            mProgressBar.setMax(value);
        }
    }

    /**
     * 返回进度条View
     *
     * @return 返回 {@link SeekBar}
     */
    public SeekBar getProgressBar() {
        return mProgressBar;
    }

    /**
     * 返回ICON图标View
     * 1. 图标可以跟随状态改变的效果，可以这么使用.
     * iconView.setImageTintMode(PorterDuff.Mode.SRC_IN);
     * iconView.setImageTintList(getColorStateList(R.color.item_color_blank_to_white_selector));
     *
     * @return 返回 {@link ImageView}
     */
    public ImageView getIconView() {
        return mImageView;
    }

    public void setImageSize(int iconSize) {
        mIconSize = iconSize;
        LayoutParams iconLayoutParams = (LayoutParams) mImageView.getLayoutParams();
        if (iconSize >= 0) {
            iconLayoutParams.width = (int) mIconSize;
            iconLayoutParams.height = (int) mIconSize;
        }
        mImageView.setLayoutParams(iconLayoutParams);
    }

    /**
     * 设置ImageView的 TintList {@link ColorStateList} 与 Mode {@link PorterDuff.Mode}
     *
     * @param tint     {@link ColorStateList}
     * @param tintMode {@link PorterDuff.Mode}
     */
    public void setImageTintListAndMode(ColorStateList tint, PorterDuff.Mode tintMode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && null != mImageView) {
            mImageView.setImageTintList(tint);
            mImageView.setImageTintMode(tintMode);
        }
    }

    /**
     * 设置箭头的 TintList {@link ColorStateList} 与 Mode {@link PorterDuff.Mode}
     *
     * @param tint     {@link ColorStateList}
     * @param tintMode {@link PorterDuff.Mode}
     */
    public void setArrowTintListAndMode(ColorStateList tint, PorterDuff.Mode tintMode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (null != mLeftArrowIv) {
                mLeftArrowIv.setImageTintList(tint);
                mLeftArrowIv.setImageTintMode(tintMode);
            }
            if (null != mRightArrowIv) {
                mRightArrowIv.setImageTintList(tint);
                mRightArrowIv.setImageTintMode(tintMode);
            }
        }
    }

    /**
     * 设置ItemView 最右侧的图标是否显示
     * @param visibility 隐藏 {@link View#GONE}，显示 {@link View#VISIBLE}
     */
    public void setImageViewVisibility(int visibility) {
        if (null != mImageView) {
            mImageView.setVisibility(visibility);
        }
    }

    /**
     * 设置图标
     *
     * @param drawable {@link Drawable}
     */
    public void setImageDrawable(Drawable drawable) {
        if (null != mImageView) {
            mImageView.setImageDrawable(drawable);
        }
    }

    /**
     * 获取标题的文本
     */
    public CharSequence getTitleText() {
        if (null != mTextView) {
            return mTextView.getText();
        }
        return "";
    }

    /**
     * 设置标题的文本
     */
    public void setText(CharSequence text) {
        if (null != mTextView) {
            mTextView.setText(text);
        }
    }

    /**
     * 设置标题字体大小
     */
    public void setTextSize(float size) {
        if (null != mTextView) {
            mTextView.setTextSize(size);
        }
    }

    public void setTextMaxWidth(int width) {
        if (null != mTextView) {
            mTextView.setMaxWidth(width);
        }
    }

    /**
     * 获取详情文本
     */
    public CharSequence getDetailText() {
        if (null != mDetailTextView) {
            return mDetailTextView.getText();
        }
        return "";
    }

    /**
     * 设置详情文本
     */
    public void setDetailText(CharSequence text) {
        if (!TextUtils.isEmpty(text) && null != mDetailTextView){
            mDetailTextView.setText(text);
        }
    }

    /**
     * 详情最大宽度
     * @param width
     */
    public void setDetailMaxWidth(int width) {
        if (null != mDetailTextView) {
            mDetailTextView.setMaxWidth(width);
        }
        if (null != mPbTextView) {
            mPbTextView.setMaxWidth(width);
        }
    }

    /**
     * 设置详情的文本字体大小
     */
    public void setDetailTextSize(float size) {
        if (null != mTextView) {
            mTextView.setTextSize(size);
        }
    }

    public void setTextWidth(int width) {
        if (null != mTextView) {
            mTextView.getLayoutParams().width = width;
        }
     }

    /**
     * 设置详情TextView的宽度
     * @param width
     */
     public void setDetailTextWidth(int width) {
         if (null != mDetailTextView) {
             mDetailTextView.getLayoutParams().width = width;
         }
     }

    /**
     * 获取Item类型
     */
    public int getItemType() {
        return mItemType;
    }

    /**
     * 设置Item类型
     *
     * @param type ITEM_TYPE_ARROW 等类型
     */
    public void setItemType(int type) {
//        if (null != mTypeView) {
//            mTypeView.removeAllViews();
//        }
        mItemType = type;
        if (type == ITEM_TYPE_NULL) {
            return;
        }
        // 避免出现双箭头切换到其它类型，还显示其它效果.
//        resetItemViewState();
        // 处理对应类型的布局界面.
//        switch (type) {
//            case ITEM_TYPE_DOUBLE_ARROW:
//            case ITEM_TYPE_ARROW: // 右边的箭头
//                if (null != mTypeView) {
//                    mRightArrowIv = getTypeImageView();
//                    mRightArrowIv.setImageDrawable(mArrowDrawable);
//                    mTypeView.addView(mRightArrowIv);
//                    LinearLayout.LayoutParams arrowLayoutParams = (LinearLayout.LayoutParams) mRightArrowIv.getLayoutParams();
//                    arrowLayoutParams.gravity = Gravity.CENTER_VERTICAL;
//                    // 双箭头方式设置
//                    if (type == ITEM_TYPE_DOUBLE_ARROW) {
//                        mLeftArrowIv.setVisibility(View.VISIBLE);
//                        mLeftArrowIv.setImageDrawable(mArrowDrawable);
//                        mLeftArrowIv.setRotation(180);
//                        LinearLayout.LayoutParams arrowLeftLayoutParams = (LinearLayout.LayoutParams) mLeftArrowIv.getLayoutParams();
//                        arrowLeftLayoutParams.rightMargin = ((LinearLayout.LayoutParams) mTypeView.getLayoutParams()).leftMargin;
//                    }
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        setArrowTintListAndMode(mArrowColorStateList, PorterDuff.Mode.SRC_IN);
//                    }
//                    mTypeView.setVisibility(VISIBLE);
//                }
//                break;
//            case ITEM_TYPE_RADIO: // 单选按钮
//            case ITEM_TYPE_SWITCH:  // 开关
//                if (null != mTypeView) {
//                    mSwitch = getCheckBoxView(mItemType == ITEM_TYPE_RADIO ? mRadioDrawableRes : mSwitchDrawableRes);
//                    //
//                    mTypeView.addView(mSwitch);
//                    mTypeView.setVisibility(VISIBLE);
//                }
//                break;
//            case ITEM_TYPE_PROGRESS: // 进度
//                if (null != mPbTextView) {
//                    mPbTextView.setVisibility(View.VISIBLE);
//                }
//                if (null != mProgressBar) {
//                    mProgressBar.setVisibility(View.VISIBLE);
//                }
//                break;
//            case ITEM_TYPE_LOADING: // 加载
////                mLoadingView = (ImageView) mLoadingViewStub.inflate();
////                mLoadingView.setVisibility(View.VISIBLE);
//                setLoadVisible(View.VISIBLE);
//                break;
//            case ITEM_TYPE_EDITTEXT: // 输入框 // 带右侧编辑框
//                initEditText();
//                break;
//            case ITEM_TYPE_CUSTOM: // 自定义
////                mTypeView.setVisibility(VISIBLE);
//                break;
//            case ITEM_TYPE_NONE: // 默认类型(只有图标 + 标题 + 详情)
//
//                if (null != mImageView) {
//                    mImageView.setVisibility(VISIBLE);
//                }
//                break;
//        }

        //
        initItemType(type);
        // 初始化相关属性
        switch (type) {
            case ITEM_TYPE_WIFI: // WIFI类型
            case ITEM_TYPE_DOUBLE_ARROW: // 双箭头
                mLeftArrowIv.setImageDrawable(mArrowDrawable);
                ConstraintLayout.LayoutParams arrowLeftLayoutParams = (ConstraintLayout.LayoutParams) mLeftArrowIv.getLayoutParams();
                arrowLeftLayoutParams.rightMargin = (int) mTypeMarginLeft; // 设置左边箭头与文本的间距
            case ITEM_TYPE_ARROW: // 右边的箭头
                mRightArrowIv.setImageDrawable(mArrowDrawable);
                ConstraintLayout.LayoutParams detailTextLayoutParams = (ConstraintLayout.LayoutParams) mDetailTextView.getLayoutParams();
                detailTextLayoutParams.rightMargin = (int) mTypeMarginLeft; // 设置详情文本与右边箭头的间距
                // 双箭头方式设置
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    setArrowTintListAndMode(mArrowColorStateList, PorterDuff.Mode.SRC_IN);
                }
                break;
            case ITEM_TYPE_RADIO: // 单选按钮
                mSwitch.setButtonDrawable(getResources().getDrawable(mRadioDrawableRes));
            case ITEM_TYPE_SWITCH:  // 开关
                if (ITEM_TYPE_SWITCH == type) {
                    mSwitch.setButtonDrawable(getResources().getDrawable(mSwitchDrawableRes));
                }
                // 使用详情文本的属性来设置CheckBox
                if (mDetailMaxWidth > 0) {
                    mSwitch.setMaxWidth((int) mDetailMaxWidth);
                }
//                mCheckBox.setTextSize(mDetailTextView.getTextSize());
//                if (null != mDetailTextView.getTextColors()) {
//                    mCheckBox.setTextColor(mDetailTextView.getTextColors());
//                }
        }
    }

    private void initItemType(int itemType) {
        Log.d("hailong.qiu", "initItemType itemType:" + itemType + " item child count:" + getChildCount());
        // 初始化控件
        new TVUICommonItemViewBuilder().create(itemType, this);

        mImageView = (ImageView) findViewById(R.id.common_item_iv); // 左侧图标
//        mTextContainer = (LinearLayout) findViewById(R.id.common_item_text_container);
        mTextView = (TextView) findViewById(R.id.common_item_tv);
//        mTextSpace = (Space) findViewById(R.id.common_item_space);

        mLeftArrowIv = (ImageView) findViewById(R.id.common_item_left_arrow_iv); // 左边箭头
        mDetailTextView = (TextView) findViewById(R.id.common_item_detail_tv); // 详情
        mRightArrowIv = (ImageView) findViewById(R.id.common_item_right_arrow_iv); // 右边箭头

        mSwitch = findViewById(R.id.common_item_check); // 开关/单选

//        mProgressBar = findViewById(R.id.common_item_pb);
//        mPbTextView = findViewById(R.id.common_item_pb_tv);
//        mTypeView = findViewById(R.id.common_item_type_view);
        mLoadingViewStub = findViewById(R.id.common_item_loading_tip); // 进度条 ViewStub 布局
//        mRightInputEt = findViewById(R.id.common_item_input_et);

        if (null != mImageView) {
            // 设置图标信息
            setImageDrawable(mIconDrawable);
            if (mIconSize > 0) {
                ConstraintLayout.LayoutParams iconLayoutParams = (ConstraintLayout.LayoutParams) mImageView.getLayoutParams();
                iconLayoutParams.width = (int) mIconSize;
                iconLayoutParams.height = (int) mIconSize;
            }
        }

        if (null != mTextView && null != mImageView) { // 设置图标与文字之间的间距
            ConstraintLayout.LayoutParams textLayoutParams = (ConstraintLayout.LayoutParams) mTextView.getLayoutParams();
            textLayoutParams.leftMargin = (int) mIconMarginRight;
        }

        if (null != mIconColorStateList) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setImageTintListAndMode(mIconColorStateList, PorterDuff.Mode.SRC_IN);
            }
        }
        // 设置文本信息
        if (mTextMaxWidth > 0) {
            setTextMaxWidth((int) mTextMaxWidth);
        }
        if (mTextWidth > 0) {
            setTextWidth((int) mTextWidth);
        }
//        setText(text);
        if (mDetailMaxWidth > 0) {
            setDetailMaxWidth((int) mDetailMaxWidth);
        }
        if (mDetailWidth > 0) {
            setDetailTextWidth((int) mDetailWidth);
        }
        // 初始化一些空间类型的数据.
//        if (itemType == ITEM_TYPE_RADIO || itemType == ITEM_TYPE_SWITCH) {
//            setChecked(isChecked);
//        } else if (itemType == ITEM_TYPE_DOUBLE_ARROW) {
//            setDoubleArrowArray(mStrArrayRes);
//        }
//        if (iconDrawable == null) {
//            setImageViewVisibility(View.GONE);
//        }
    }

    protected void resetItemViewState() {
        if (null != mLeftArrowIv) {
            mLeftArrowIv.setVisibility(View.GONE);
        }
        if (null != mProgressBar) {
            mProgressBar.setVisibility(View.GONE);
        }
        if (null != mPbTextView) {
            mPbTextView.setVisibility(View.GONE);
        }
        if (null != mImageView) {
            mImageView.setVisibility(GONE);
        }
    }

    /**
     * 设置 key 按键的事件监听
     */
    public void setOnItemKeyListener(View.OnKeyListener cb) {
        setOnKeyListener(cb);
    }

    View.OnClickListener mOnClickListener;
    OnFocusChangeListener mOnFocusChangeListener;

    /**
     * 设置单击事件监听
     */
    public void setOnItemClickListener(View.OnClickListener cb) {
        mOnClickListener = cb;
    }

    /**
     * 设置焦点事件监听
     */
    public void setOnItemFocusChangeListener(OnFocusChangeListener cb) {
        mOnFocusChangeListener = cb;
    }

    /**
     * 设置选中事件监听
     */
    public void setOnItemCheckedChangeListener(final CompoundButton.OnCheckedChangeListener cb) {
        if (null != mSwitch) {
            mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    cb.onCheckedChanged(buttonView, isChecked);
                }
            });
        }
    }

    /**
     * 设置界面无法操作
     * @param enabled false 无法操作界面 true 反之
     */
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        // 无任何操作，焦点也要失去，不能上焦点.
        setFocusable(enabled);
        setFocusableInTouchMode(enabled);
    }

    private int mGroupType = 0;

    /**
     * 组类型，主要用于单选按钮类型
     * @param groupType
     */
    public void setGroupType(int groupType) {
        this.mGroupType = groupType;
    }

    public int getGroupType() {
        return mGroupType;
    }

    /**
     * 设置开关状态
     */
    public void setChecked(boolean isChecked) {
        if (null != mSwitch) {
            mSwitch.setChecked(isChecked);
        }
//        misChecked = isChecked;
    }

    /**
     * 选中状态
     */
    public boolean isChecked() {
        if (null != mSwitch) {
            return mSwitch.isChecked();
        }
        return false;
    }

    /**
     * 更换开关状态
     */
    public void toggle() {
        // radio 防止重置点击
        if (isChecked() && mItemType == ITEM_TYPE_RADIO) {
            return;
        }
        // 设置状态
        setChecked(!isChecked());
    }

    /**
     * 设置标题文本为粗体
     */
    public void setTextViewBold(boolean isBold) {
    }

    /**
     * 获取输入文本
     */
    public CharSequence getInputText() {
        if (null != mRightInputEt) {
            return this.mRightInputEt.getText();
        }
        return "";
    }

    private int mMaxLen = 0;

    public int getMaxLen() {
        return mMaxLen;
    }

    /**
     * 设置输入最大的字符个数.
     */
    public void setMaxLength(int len) {
        this.mMaxLen = len;
        if (mMaxLen > 0) {
            if (null != mRightInputEt) {
                mRightInputEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(mMaxLen + 1)});
            }
        }
    }

    /**
     * 设置 EditText监听事件.
     */
    public void setOnEditorActionListener(TextView.OnEditorActionListener ealcb) {
        if (null != mRightInputEt) {
            mRightInputEt.setOnEditorActionListener(ealcb);
        }
    }

    public void setEditTextSize(float size) {
        if (null != mRightInputEt) {
            mRightInputEt.setTextSize(size);
        }
    }

    /**
     * 设置inputType类型.
     */
    public void setInputType(int type) {
        if (null != mRightInputEt) {
            mRightInputEt.setInputType(type);
        }
    }

    /**
     * 设置过滤文本. (EditText). 比如 0123456789.
     * 参考文章: http://stackoverflow.com/questions/7300490/set-edittext-digits-programmatically
     */
    public void setDigits(String digits) {
        if (!TextUtils.isEmpty(digits)) {
            mRightInputEt.setKeyListener(DigitsKeyListener.getInstance(digits));
        }
    }

    /**
     * 是否显示密码
     * @param show true 密码方式显示 false 反之
     */
    public void setShowPassText(boolean show) {
        if (show) {
            mRightInputEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            mRightInputEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
    }

    private void initEditText() {
        EditText et = mRightInputEt;
//        if (null != mDetailColor) {
//            et.setTextColor(mDetailColor);
//        }
//        et.setTextSize(mDetailSize);
        setDigits(mDigits);
        setInputType(mInputType);
        //        et.setHint("|");
//        et.setHintTextColor();
//        LinearLayout.LayoutParams wifiLayoutParams = (LinearLayout.LayoutParams) et.getLayoutParams();
//        wifiLayoutParams.rightMargin = ((LayoutParams)mTypeView.getLayoutParams()).leftMargin;
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();
                if ((getMaxLen() != 0) && (length > getMaxLen())) {
                    try {
                        // 去掉多余的字符串.
//                    mRightInputEt.setText(s.delete(s.length() - 1, s.length()));
                        int start = mRightInputEt.getSelectionStart();
                        int end = mRightInputEt.getSelectionEnd();
                        mRightInputEt.setText(s.delete(start - 1, start));
                        mRightInputEt.setSelection(start - 1);
                        // 提示.
//                            showMsg(R.string.input_max_tip_msg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (null != mRightInputEt) {
                    mRightInputEt.requestFocusFromTouch();
                    setSelected(hasFocus);
                }
            }
        });
        mRightInputEt.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                setSelected(hasFocus);
                if (null != mOnFocusChangeListener) {
                    mOnFocusChangeListener.onFocusChange(v, hasFocus);
                }
            }
        });
        mRightInputEt.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (event.getKeyCode()) {
                        case KeyEvent.KEYCODE_DPAD_UP:
                        case KeyEvent.KEYCODE_DPAD_DOWN:
                            ViewGroup rootView = (ViewGroup) getRootView();
                            if (rootView != null) {
                                View upView = FocusFinder.getInstance().findNextFocus(rootView, v, event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP ? View.FOCUS_UP : View.FOCUS_DOWN);
                                if (upView != null) {
                                    upView.requestFocusFromTouch();
                                    return true;
                                }
                            }
                    }
                }
                return false;
            }
        });
        et.setVisibility(View.VISIBLE);
    }

    public void setArrowImage(int res) {
//        if (null != mTypeView) {
        if (mRightArrowIv == null) {
            mRightArrowIv = getTypeImageView();
            mTypeView.addView(mRightArrowIv);
        }
        mRightArrowIv.setImageResource(res);
//        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // 只有双箭头与进度条才会触发按键.
        if (mItemType == ITEM_TYPE_DOUBLE_ARROW) {
            if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                setLeft2RightDoubleArrowValue(-1);
                return true;
            } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                setLeft2RightDoubleArrowValue(1);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 设置双箭头的值.
     */
    private void setLeft2RightDoubleArrowValue(int setup) {
        int index = mDoubleArrowCurrentIndex + setup;
        if (index < 0) {
            index = mDoubleArrowItems.size() - 1;
        }
        if (index >= mDoubleArrowItems.size()) {
            index = 0;
        }
        // 设置数值，调用回调.
        if (setDoubleArrowIndex(index) && null != mOnDoubleArrowViewListener) {
            mOnDoubleArrowViewListener.onDoubleArrowSwitch(this, index);
        }
    }

    List<String> mDoubleArrowItems = new ArrayList<>();
    int mDoubleArrowCurrentIndex = 0;
    OnDoubleArrowViewListener mOnDoubleArrowViewListener;

    public static interface OnDoubleArrowViewListener {
        void onDoubleArrowSwitch(TVUICommonItemView itemView, int index);
    }

    public void setOnDoubleArrowViewListener(OnDoubleArrowViewListener cb) {
        mOnDoubleArrowViewListener = cb;
    }

    public void setDoubleArrowArray(int min, int max) {
        List<String> items = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            items.add(i + "");
        }
        setDoubleArrowArray(items);
    }

    /**
     * 设置双箭头中间的数据组
     */
    public void setDoubleArrowArray(List<String> items) {
        mDoubleArrowItems.clear();
        mDoubleArrowItems.addAll(items);
        setDoubleArrowIndex(mDoubleArrowCurrentIndex);
    }

    /**
     * 设置双箭头中间的数据组
     */
    public void setDoubleArrowArray(int arrayRes) {
        if (arrayRes != -1) {
            String[] datas = getResources().getStringArray(arrayRes);
            if (null != datas) {
                setDoubleArrowArray(Arrays.asList(datas));
            }
        }
    }

    /**
     * 设置双箭头中间的值
     */
    public boolean setDoubleArrowIndex(int index) {
        if (index >= 0 && index < mDoubleArrowItems.size()) {
            mDoubleArrowCurrentIndex = index;
            String text = mDoubleArrowItems.get(index);
            if (null != mDetailTextView) {
                mDetailTextView.setText(text);
            }
            return true;
        }
        return false;
    }

    /**
     * 设置 CheckBox 文本
     * @param text 设置文本
     */
//    public void setCheckText(String text, boolean isHideIcon) {
    public void setCheckText(String text) {
        if (mSwitch != null) {
            mSwitch.setText(text);
            // 由于特殊性，如果非得使用文本切换，这里去除掉图标样式
//        if (isHideIcon) {
            mSwitch.setButtonDrawable(null);
//        }
        }
    }

    public CheckBox getCheckBox() {
        return mSwitch;
    }

    /**
     * 添加自定义控件
     */
    public void addTypeCustomView(View view, int index) {
        if (mItemType == ITEM_TYPE_CUSTOM) {
            mTypeView.addView(view, index);
            mTypeView.setVisibility(View.VISIBLE);
        }
    }

    public void addTypeCustomView(View view) {
        if (mItemType == ITEM_TYPE_CUSTOM) {
            mTypeView.addView(view);
            mTypeView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 是否显示进度加载
     * @param visible
     */
    public void setLoadVisible(int visible) {
        if (null != mLoadingViewStub) {
            if (null == mLoadingView) {
                mLoadingView = (ProgressBar) mLoadingViewStub.inflate();
            }
            mLoadingView.setVisibility(visible);
            if (null != mIconColorStateList && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mLoadingView.setProgressTintList(mIconColorStateList);
                mLoadingView.setProgressTintMode(PorterDuff.Mode.SRC_IN);
            }
            // 隐藏后面布局的控件，只显示进度条
//            mTypeView.setVisibility(visible == View.GONE ? View.VISIBLE : View.GONE);
            setVisibleViews(visible == View.GONE ? View.VISIBLE : View.GONE);
        }
    }

    private void setVisibleViews(int visible) {
        if (null != mSwitch) {
            mSwitch.setVisibility(visible);
        }
        if (null != mRightArrowIv) {
            mRightArrowIv.setVisibility(visible);
        }
    }

    protected ViewGroup.LayoutParams getAccessoryLayoutParams() {
        return getAccessoryLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    protected ViewGroup.LayoutParams getAccessoryLayoutParams(int w, int h) {
        return new ViewGroup.LayoutParams(w > 0 ? w : ViewGroup.LayoutParams.WRAP_CONTENT, h > 0 ? h : ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    protected ImageView getTypeImageView(int w, int h) {
        ImageView resultImageView = new ImageView(getContext());
        resultImageView.setLayoutParams(getAccessoryLayoutParams(w, h));
        resultImageView.setScaleType(ImageView.ScaleType.CENTER);
        // duplicateParentState=true，接收父状态
        resultImageView.setDuplicateParentStateEnabled(true);
        return resultImageView;
    }

    protected ImageView getTypeImageView() {
        return getTypeImageView(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    protected CheckBox getCheckBoxView(int res) {
        CheckBox checkBox = new CheckBox(getContext());
        if (res > 0) {
            checkBox.setButtonDrawable(getResources().getDrawable(res));
        }
        checkBox.setLayoutParams(getAccessoryLayoutParams());
        // duplicateParentState=true，接收父状态
        checkBox.setDuplicateParentStateEnabled(true);
        // 通过整个item的点击事件来更新CheckBox开关的状态
        checkBox.setClickable(false);
        checkBox.setEnabled(false);
        checkBox.setFocusable(false);
        checkBox.setFocusableInTouchMode(false);
        // 使用详情文本的属性来设置CheckBox
        if (mDetailMaxWidth > 0) {
            checkBox.setMaxWidth((int) mDetailMaxWidth);
        }
        checkBox.setTextSize(mDetailTextView.getTextSize());
        if (null != mDetailTextView.getTextColors()) {
            checkBox.setTextColor(mDetailTextView.getTextColors());
        }
        return checkBox;
    }

    /**
     * 处理其它界面取消选中的状态
     */
    private void resetAllCheckState() {
        if (mItemType == TVUICommonItemView.ITEM_TYPE_RADIO) {
            ViewGroup vg = (ViewGroup) TVUICommonItemView.this.getParent();
            if (vg != null && isChecked()) {
                int childCount = vg.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childView = vg.getChildAt(i);
                    if (childView instanceof TVUICommonItemView && !childView.equals(TVUICommonItemView.this)) {
                        TVUICommonItemView itemView = (TVUICommonItemView) childView;
                        // 确定是单选类型 并且 为 同一个组类型.
                        if (itemView.getItemType() == TVUICommonItemView.ITEM_TYPE_RADIO && itemView.getGroupType() == getGroupType()) {
                            itemView.setChecked(false);
                        }
                    }
                }
            }
        }
    }

    /**
     * 调用 setAlpha，View 绘制到 offscreen buffer 中 并做透明度的转化，最后显示再屏幕上.(这个过程很耗费性能)
     * 避免这个过程可以分很多种情况，常见的比如没有背景的TextView，就可以直接设置文字颜色，
     * 而不是设置整体alpha；ImageView设置图片透明度setImageAlpha，自定义View设置绘制时的paint的透明度。
     * Avoids creating a hardware layer when animating alpha.
     * https://blog.csdn.net/xingchenxuanfeng/article/details/56488045
     */
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }

    /**
     * 设置焦点属性
     */
    public void setFocusValues(boolean isFocus) {
        setFocusableInTouchMode(isFocus);
        setFocusable(isFocus);
    }

    public void setTextColor(int color) {
        if (null != mTextView) {
            mTextView.setTextColor(color);
        }
    }

    public void setTextColor(ColorStateList colorList) {
        if (null != mTextView) {
            mTextView.setTextColor(colorList);
        }
    }

    public void setDetailTextColor(int color) {
        if (null != mDetailTextView) {
            mDetailTextView.setTextColor(color);
        }
    }

    public void setDetailVisibility(int visibility) {
        if (null != mDetailTextView) {
            mDetailTextView.setVisibility(visibility);
        }
    }

    public void setDetailTextColor(ColorStateList colorList) {
        if (null != mDetailTextView) {
            mDetailTextView.setTextColor(colorList);
        }
    }

    private static SimpleArrayMap<String, Integer> skinAttrs;

    static {
        skinAttrs = new SimpleArrayMap<>(5);
        skinAttrs.put(SkinManager.IMAGE_TINT_TAG, R.attr.tvui_commonItem_ImageTint_theme);
        skinAttrs.put(SkinManager.ARROW_TINT_TAG, R.attr.tvui_commonItem_ArrowTint_theme);
        skinAttrs.put(SkinManager.TEXT_COLOR_TAG, R.attr.tvui_main_first_text_color);
        skinAttrs.put(SkinManager.SECOND_TEXT_COLOR_TAG, R.attr.tvui_main_second_text_color);
        skinAttrs.put(SkinManager.BACKGROUND_TAG, R.attr.background);
    }

    @Override
    public SimpleArrayMap<String, Integer> getSkinAttr() {
        return skinAttrs;
    }

    @Override
    public void addFocusables(ArrayList<View> views, int direction, int focusableMode) {
        View prentView = (View) getParent();

        // BUG:Leanback 的不处理isEnabled=false, getVisibility=View.GONE 的view，依然可以上焦.
        // 处理置灰与隐藏的view 还可以上焦点问题
        if ((!isEnabled() || getVisibility() == View.GONE) && null != prentView && prentView instanceof RecyclerView) {
            RecyclerView tv = (RecyclerView) prentView;
            int position = tv.getChildLayoutPosition(this);
            // 拿自己 相邻的上面与下面的 itemView.
            View nextView = null;

            for (int i = position; i >= 0; i--) {
                final View tempView = tv.getChildAt(i);
                if (tempView != null && tempView.isEnabled() && tempView.getVisibility() == View.VISIBLE) {
                    nextView = tempView;
                    break;
                }
            }

            for (int i = position; i < getChildCount(); i++) {
                final View tempView = tv.getChildAt(i);
                if (tempView != null && tempView.isEnabled() && tempView.getVisibility() == View.VISIBLE) {
                    nextView = tempView;
                    break;
                }
            }

            // 如果只剩下一个置灰的item，那么焦点还是停留在原来的地方
            if (nextView == null && views.size() > 0) {
                nextView = views.get(0);
            }

            // 如果还没有，TMD，那就加自己去，去死吧！！！这里是容错机制，一般不会触发.
            if (nextView == null) {
                nextView = prentView;
            }
            views.add(nextView);
        } else {
            super.addFocusables(views, direction, focusableMode);
        }
    }

    @Override
    public boolean requestFocus(int direction, Rect previouslyFocusedRect) {
        return super.requestFocus(direction, previouslyFocusedRect);
    }

}
