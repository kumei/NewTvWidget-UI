package com.open.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import androidx.collection.SimpleArrayMap;

import com.open.ui.R;
import com.open.ui.skin.ISkinLayout;

/**
 * 参考QMUI的思路
 */
@SuppressLint("AppCompatCustomView")
public class TVUIButton extends Button implements ISkinLayout {

    TVUIRoundDrawable mUIRoundDrawable;

    public TVUIButton(Context context) {
        this(context, null);
    }

    public TVUIButton(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.TVUIRoundButtonStyle);
    }

    TVUILayoutHeper mTvuiLayoutHeper;

    public TVUIButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mUIRoundDrawable = TVUIRoundDrawable.build(context, attrs, defStyleAttr);
        // 设置背景
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(mUIRoundDrawable);
        } else {
            setBackgroundDrawable(mUIRoundDrawable);
        }
        // 设置 padding
//        setPadding(0, 0, 0, 0);
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
        mTvuiLayoutHeper = new TVUILayoutHeper(context, attrs, defStyleAttr, this);
    }

    @Override
    public void setBackgroundColor(int color) {
        mUIRoundDrawable.setBgData(ColorStateList.valueOf(color));
    }

    public void setBackgroundColor(ColorStateList colorStateList) {
        mUIRoundDrawable.setBgData(colorStateList);
    }

    @Override
    public SimpleArrayMap<String, Integer> getSkinAttr() {
        return null;
    }

}
