package com.open.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;

/**
 * 加载View
 */
@SuppressLint("AppCompatCustomView")
public class TVUILoadingView extends ImageView {

    public TVUILoadingView(Context context) {
        super(context);
    }

    public TVUILoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TVUILoadingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
