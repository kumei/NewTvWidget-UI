package com.open.ui.widget.dialog;

import android.view.View;

import com.open.ui.widget.TVUIButton;

/**
 * 保存按钮信息
 */
public class TVUIDialogButtonAction {

    private OnCallBackListener mOnCallBackListener;
    private String mText;

    public TVUIDialogButtonAction() {
    }

    public TVUIDialogButtonAction text(String text) {
        mText = text;
        return this;
    }

    public TVUIDialogButtonAction onCallBack(OnCallBackListener cb) {
        mOnCallBackListener = cb;
        return this;
    }

    public interface OnCallBackListener {
        void onCallBack(TVUIDialog dialog, View view);
    }

    public TVUIButton onCreateButton(final TVUIDialog dialog) {
        TVUIButton button = new TVUIButton(dialog.getContext());
        int padding = 0;
        button.setPadding(padding, 0, padding, 0);
        button.setText(mText);
        button.setClickable(true);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mOnCallBackListener) {
                    mOnCallBackListener.onCallBack(dialog, v);
                }
            }
        });
        return button;
    }

}
