package com.open.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * @Author: jianping.qiao 253386581@qq.com
 * @Maintainer: jianping.qiao 253386581@qq.com
 * @Date: 2020/3/9
 * @Copyright: 2020 www.andriodtvdev.com Inc. All rights reserved.
 *
 * Setting Dialog 基类，设置一些Dialog基本属性
 */
public abstract class BaseDialog extends Dialog {

    protected View mMainView;

    public BaseDialog(Context context) {
        super(context);
    }

    public BaseDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public BaseDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(getBackgroundColor()));
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.addFlags(
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                        | WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        this.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.type = WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG;
        lp.flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        lp.format = PixelFormat.TRANSLUCENT;
        lp.gravity = setGravity();
        WindowManager wm = (WindowManager) (getContext().getSystemService(Context.WINDOW_SERVICE));
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        lp.windowAnimations = -1;
        window.setAttributes(lp);
        window.setSoftInputMode(getSoftInputMode());
        window.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        int layoutRes = getLayoutResource();
        if (layoutRes > 0) {
            initData();
            mMainView = View.inflate(getContext(), getLayoutResource(), null);
            initView();
            setContentView(mMainView);
        }
    }

    public int getSoftInputMode() {
        return WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING;
    }

    protected abstract void initView();

    protected abstract int getLayoutResource();

    protected abstract int setGravity();

    protected int getBackgroundColor() {
        return Color.TRANSPARENT;
    }

    public void initData(){

    }

}
