package com.open.ui.widget.dialog;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.open.ui.R;
import com.open.ui.skin.SkinManager;
import com.open.ui.widget.TVUITextView;

/**
 * TV 对话框
 */
public class TVUIDialog extends BaseDialog {

    public TVUIDialog(Context context) {
        super(context);
    }

    public TVUIDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public TVUIDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void initView() {
    }

    @Override
    protected int getLayoutResource() {
        return 0;
    }

    @Override
    protected int setGravity() {
        return Gravity.CENTER;
    }

    @Override
    protected int getBackgroundColor() {
        ColorStateList colorList = SkinManager.getAttrColorStateList(getContext(), R.attr.tvui_dialog_root_background);
        return colorList.getDefaultColor();
    }

    /**
     * 带输入框的对话框 Builder，主要用于名称，密码 等输入.
     */
    public static class EditTextDialogBuilder extends TVUIDialogBuilder<EditTextDialogBuilder> {

        private InputFilter mInputFilter;
        private int mInputType;
        private TextWatcher mTextWatcher;
        private TextView.OnEditorActionListener mOnEditorActionListener;

        /**
         * 设置 EditText 的 inputType
         */
        public EditTextDialogBuilder setInputType(int inputType) {
            mInputType = inputType;
            return this;
        }

        /**
         * 设置 EditText 的 InputFilter
         */
        public EditTextDialogBuilder setInputFilter(InputFilter inputFilter) {
            mInputFilter = inputFilter;
            return this;
        }

        public EditTextDialogBuilder setTextWatcher(TextWatcher textWatcher) {
            mTextWatcher = textWatcher;
            return this;
        }

        public EditTextDialogBuilder setOnEditorActionListener(TextView.OnEditorActionListener cb) {
            mOnEditorActionListener = cb;
            return this;
        }

        public EditTextDialogBuilder(Context context) {
            super(context);
        }

        @Override
        protected View onCreateContent(TVUIDialog dialog) {
//            EditText editText = new EditText();
            return null;
        }

    }

    public static class MessageDialogBuilder extends TVUIDialogBuilder<MessageDialogBuilder> {

        String mMessage;
        Integer mMessageColorRes;

        public MessageDialogBuilder(Context context) {
            super(context);
        }

        @Override
        protected View onCreateContent(TVUIDialog dialog) {
            TVUITextView tv = new TVUITextView(dialog.getContext());
            tv.setText(mMessage);
            tv.setTextColor(Color.BLACK);
            tv.setGravity(Gravity.CENTER_HORIZONTAL);
            int textSize = SkinManager.getAttrDimen(dialog.getContext(), R.attr.tvui_dialog_message_size);
            ColorStateList textColorList = SkinManager.getAttrColorStateList(dialog.getContext(),
                    mMessageColorRes != null ? mMessageColorRes : R.attr.tvui_dialog_message_color);
            tv.setTextSize(textSize);
            tv.setTextColor(textColorList);
            return tv;
        }

        public MessageDialogBuilder setMessageColor(int colorRes) {
            this.mMessageColorRes = colorRes;
            return this;
        }
        public MessageDialogBuilder setMessage(String msg) {
            mMessage = msg;
            return this;
        }

        public MessageDialogBuilder setMessage(int resId) {
            return setMessage(getContext().getResources().getString(resId));
        }

    }

}
