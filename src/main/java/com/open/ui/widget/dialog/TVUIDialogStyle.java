package com.open.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;

import androidx.constraintlayout.widget.ConstraintLayout;

/**
 * TVUIDialogStyle
 */
public class TVUIDialogStyle {

    /**
     * FullLayoutDialogBuilder
     * <ul>
     *     <li>通过 {@link #addLayout(View)} (View)} 设置布局内容View </li>
     * </ul>
     * <p></p>
     * <pre>
     * eg.
     * ImageView iv = new ImageView(mContext);
     * iv.setBackgroundResource(R.drawable.userabout_focus_figure);
     * new TVUIDialogStyle.FullLayoutDialogBuilder(mContext)
     *                    .addLayout(iv)
     *                    .create()
     *                    .show();
     * </pre>
     */
    public static class FullLayoutDialogBuilder extends TVUIDialogBuilder<FullLayoutDialogBuilder> {

        private View mLayout;

        public FullLayoutDialogBuilder(Context context) {
            super(context);
        }

        public FullLayoutDialogBuilder addLayout(View layout) {
            this.mLayout = layout;
            return this;
        }

        @Override
        public Dialog create() {
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundColor(Color.TRANSPARENT);
            setSpace(0);
            //
            Dialog dialog = super.create();
            return dialog;
        }

        @Override
        protected View onCreateContent(TVUIDialog dialog) {
            return mLayout;
        }

        @Override
        protected ConstraintLayout.LayoutParams onCreateContentLayoutParams(Context context) {
            ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            lp.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
            lp.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
            lp.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
            lp.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
            lp.constrainedHeight = true;
            lp.constrainedWidth = true;
            return lp;
        }

    }

}
