package com.open.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.open.ui.R;

public class TVUILinearLayout extends LinearLayout {

    public TVUILinearLayout(@NonNull Context context) {
        this(context, null);
    }

    public TVUILinearLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.TVUIShadowStyle);
    }

    public TVUILinearLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    TVUILayoutHeper mLayoutHelper;

    public void init(Context context, AttributeSet attrs, int defStyleAttr) {
        mLayoutHelper = new TVUILayoutHeper(context, attrs,defStyleAttr, this);
    }

}