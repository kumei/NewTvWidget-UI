package com.open.ui.widget.itemview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.open.ui.R;
import com.open.ui.widget.TVUICommonItemView;

//public abstract class TVUICommonItemViewBuilder<T extends TVUICommonItemViewBuilder> {
public class TVUICommonItemViewBuilder<T extends TVUICommonItemViewBuilder> {

    // TODO:临时先处理卡顿问题，后续使用声明式布局来加载界面 组合起来(图标+文本+箭头等等)
    public View create(int type, ConstraintLayout container) {
        container.removeAllViews();
        switch (type) {
            case TVUICommonItemView.ITEM_TYPE_NONE:
                // 左侧图标
                ImageView commonItemIv = createImageView(container.getContext(), R.id.common_item_iv);
                container.addView(commonItemIv, onCreateLeftIconLayoutParams());
                // 添加左侧的正文本
                TextView textView = createTextView(container.getContext(), container, R.id.common_item_tv);
                container.addView(textView, onCreateItemTextLayoutParams());
                break;
            case TVUICommonItemView.ITEM_TYPE_WIFI: // WIFI 类型
                // 左侧图标
                ImageView wifiItemIv = createImageView(container.getContext(), R.id.common_item_iv);
                container.addView(wifiItemIv, onCreateLeftIconLayoutParams());
            case TVUICommonItemView.ITEM_TYPE_DOUBLE_ARROW: // 显示两个箭头 "文本     < 详情 >"
                // 添加详情的左侧图标
                ImageView leftImageView = createImageView(container.getContext(), R.id.common_item_left_arrow_iv);
                leftImageView.setRotation(180);
                container.addView(leftImageView, onCreateLeftArrowLayoutParams());
            case TVUICommonItemView.ITEM_TYPE_ARROW: // 右边显示箭头        "文本       详情 >"
                // 添加左侧的正文本
                TextView arrowTextView = createTextView(container.getContext(), container, R.id.common_item_tv);
                container.addView(arrowTextView, onCreateItemTextLayoutParams());
                // 添加详情文本
                TextView detailTextView = createDetailTextView(container.getContext(), container, R.id.common_item_detail_tv);
                container.addView(detailTextView, onCreateItemDetailTextLayoutParams());
                // 添加详情的右侧图标
                ImageView rightImageView = createImageView(container.getContext(), R.id.common_item_right_arrow_iv);
                container.addView(rightImageView, onCreateRightArrowLayoutParams());
                break;
            case TVUICommonItemView.ITEM_TYPE_SWITCH: // 开关
            case TVUICommonItemView.ITEM_TYPE_RADIO: // 单选
                // 添加左侧的正文本
                TextView checkTextView = createTextView(container.getContext(), container, R.id.common_item_tv);
                container.addView(checkTextView, onCreateItemTextLayoutParams());
                // 添加左侧的正文本
                CheckBox checkBox = createCheckBoxView(container.getContext(), R.id.common_item_check);
                container.addView(checkBox, onCreateRightArrowLayoutParams());
                break;
            case TVUICommonItemView.ITEM_TYPE_TEXT: // 正文与详情文本类型(10)
                // 添加左侧的正文本
                TextView textView1 = createTextView(container.getContext(), container, R.id.common_item_tv);
                ConstraintLayout.LayoutParams lp1 = onCreateItemTextLayoutParams();
                lp1.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
                container.addView(textView1, lp1);
                // 添加详情文本
                TextView detailTextView2 = createDetailTextView(container.getContext(), container, R.id.common_item_detail_tv);
//                ConstraintLayout.LayoutParams lp2 = onCreateItemDetailTextLayoutParams();
                ConstraintLayout.LayoutParams lp2 = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp2.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
                lp2.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
                lp2.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
                container.addView(detailTextView2, lp2);
                break;
        }
        // 添加加载进度条
        View viewStub = LayoutInflater.from(container.getContext()).inflate(R.layout.tvui_common_item_loading_view, container, false);
        container.addView(viewStub, onCreateRightArrowLayoutParams());
        //
        return null;
    }

    private CheckBox createCheckBoxView(Context context,  int id) {
        CheckBox checkBox = new CheckBox(context);
        checkBox.setId(id);
        checkBox.setDuplicateParentStateEnabled(true);
        // 通过整个item的点击事件来更新CheckBox开关的状态
        checkBox.setClickable(false);
        checkBox.setEnabled(false);
        checkBox.setFocusable(false);
        checkBox.setFocusableInTouchMode(false);
        return checkBox;
    }

    private TextView createDetailTextView(Context context, View container, int id) {
        TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.tvui_common_item_detailtext_view, (ViewGroup) container, false);
        textView.setId(id);
        return textView;
    }

    private TextView createTextView(Context context, View container, int id) {
        TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.tvui_common_item_text_view, (ViewGroup) container, false);
        textView.setId(id);
        return textView;
    }

    private ConstraintLayout.LayoutParams onCreateItemTextLayoutParams() {
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftToRight = R.id.common_item_iv;
        lp.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        lp.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        return lp;
    }

    private ImageView createImageView(Context context, int id) {
        ImageView commonItemIv = new ImageView(context);
        commonItemIv.setId(id);
        commonItemIv.setDuplicateParentStateEnabled(true);
        commonItemIv.setAdjustViewBounds(true);
        commonItemIv.setContentDescription(null);
        commonItemIv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        return commonItemIv;
    }

    private ConstraintLayout.LayoutParams onCreateLeftIconLayoutParams() {
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
        lp.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        lp.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        return lp;
    }

    private ConstraintLayout.LayoutParams onCreateLeftArrowLayoutParams() {
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.rightToLeft = R.id.common_item_detail_tv;  // < 详情 >
        lp.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        lp.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        return lp;
    }

    private ConstraintLayout.LayoutParams onCreateItemDetailTextLayoutParams() {
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.rightToLeft = R.id.common_item_right_arrow_iv; // 对齐右侧图标  详情 >
        lp.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        lp.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        return lp;
    }

    private ConstraintLayout.LayoutParams onCreateRightArrowLayoutParams() {
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID; // >
        lp.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        lp.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        return lp;
    }

}
