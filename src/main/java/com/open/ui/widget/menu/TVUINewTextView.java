package com.open.ui.widget.menu;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * 优化TextView
 */
public class TVUINewTextView extends View {

    public TVUINewTextView(Context context) {
        this(context, null);
    }

    public TVUINewTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TVUINewTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}
