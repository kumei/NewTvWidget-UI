package com.open.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.collection.SimpleArrayMap;

import com.open.ui.R;
import com.open.ui.skin.ISkinLayout;
import com.open.ui.skin.SkinManager;

@SuppressLint("AppCompatCustomView")
public class TVUITextView extends TextView implements ISkinLayout {

    public TVUITextView(Context context) {
        super(context);
    }

    public TVUITextView(Context context, AttributeSet attrs) {
        super(context, attrs,R.attr.TVUITextViewStyle);
    }

    public TVUITextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // 获取属性值
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TVUITextView, defStyleAttr, 0);
        String text = array.getString(R.styleable.TVUITextView_tvui_textview_titleText);
        float textSize = array.getDimension(R.styleable.TVUITextView_tvui_textview_titleTextSize, 12);
        ColorStateList textColor = array.getColorStateList(R.styleable.TVUITextView_tvui_textview_titleColor);
        float textMaxWidth = array.getDimension(R.styleable.TVUITextView_tvui_textview_titleTextMaxWidth, 0);
        array.recycle();

        setText(text);
        setTextSize(textSize);
        setTextColor(textColor);
        if (textMaxWidth > 0) {
            setMaxWidth((int) textMaxWidth);
        }
    }

    private static SimpleArrayMap<String, Integer> skinAttrs;

    static {
        skinAttrs = new SimpleArrayMap<>(1);
        skinAttrs.put(SkinManager.SECOND_TEXT_COLOR_TAG, R.attr.tvui_main_second_text_color);
    }

    @Override
    public SimpleArrayMap<String, Integer> getSkinAttr() {
        return skinAttrs;
    }

}
