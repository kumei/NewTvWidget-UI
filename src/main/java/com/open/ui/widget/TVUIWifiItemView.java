package com.open.ui.widget;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.open.ui.R;

/**
 * WIFI Item View
 * 包含名称，信号强度，锁，状态等信息
 */
public class TVUIWifiItemView extends TVUICommonItemView {

    public TVUIWifiItemView(Context context) {
        this(context, null);
    }

    public TVUIWifiItemView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.TVUICommonItemViewStyle);
    }

    public TVUIWifiItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    ImageView mWifiLevelIv;
    ImageView mWifiLockIv;

    private void init() {
        mWifiLevelIv = mLeftArrowIv;
        mWifiLevelIv.setRotation(0);
        setWifiImageViewTint(mWifiLevelIv);
        mWifiLevelIv.setImageResource(R.drawable.wifi_level_selector);
        // 修改图标为 WIFI锁图标
        mWifiLockIv = mImageView;
        if (null != mWifiLockIv) {
            // 设置锁的属性
            mWifiLockIv.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
            mWifiLockIv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            //
            setWifiImageViewTint(mWifiLockIv);
            mWifiLockIv.setImageResource(R.drawable.wifi_level_selector);
        }
    }

    /**
     * 设置 信号强度 资源图片
     */
    public void setWifiLevelResource(int resId) {
        if (null != mWifiLevelIv) {
            mWifiLevelIv.setImageResource(resId);
        }
    }

    public void setWifiLockResource(int resId) {
        if (null != mWifiLockIv) {
            mWifiLockIv.setImageResource(resId);
        }
    }
    /**
     * WIFI的锁
     */
    public void setWifiLockShow(boolean isShow) {
        if (null != mWifiLockIv) {
            mWifiLockIv.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
        }
    }

    public void setWifiLevelShow(boolean isShow) {
        if (null != mWifiLevelIv) {
            mWifiLevelIv.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
        }
    }

    /**
     * 获取WIFI ImageView
     */
    private void setWifiImageViewTint(ImageView wifiImageView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && null != wifiImageView) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                ColorStateList imgColorStateList = SkinManager.getInstance(getContext()).getAttrColorStateList(getContext(), getContext().getTheme(), R.attr.tvui_commonItem_ArrowTint);
                wifiImageView.setImageTintList(mIconColorStateList);
//                wifiImageView.setImageTintList(getContext().getColorStateList(R.color.tvui_item_icon_color_selector));
            }
            wifiImageView.setImageTintMode(PorterDuff.Mode.SRC_IN);
        }
    }

}
