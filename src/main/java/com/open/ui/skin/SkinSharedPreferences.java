package com.open.ui.skin;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SkinSharedPreferences {

    public static final String SKIN_NAME = "SkinManager";
    public static final String SKIN_KEY = "StyleName";

    private SkinSharedPreferences() {
        throw new AssertionError();
    }

    public static boolean putString(Context context, String value) {
        SharedPreferences settings;
        if (android.os.Build.VERSION.SDK_INT >= 26){
            settings = context.getSharedPreferences(SKIN_NAME, MODE_PRIVATE);
        }else {
            settings = context.getSharedPreferences(SKIN_NAME, Context.MODE_WORLD_READABLE | Context.MODE_WORLD_WRITEABLE);
        }
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SKIN_KEY, value);
        return editor.commit();
    }

    public static String getString(Context context) {
        return getString(context, "");
    }

    public static String getString(Context context, String defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(SKIN_NAME, Context.MODE_MULTI_PROCESS);
        return settings.getString(SKIN_KEY, defaultValue);
    }


}
