package com.open.ui;

import android.app.Dialog;
import android.view.View;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.open.ui.widget.TVUICommonItemView;
import com.open.ui.widget.TVUILinearLayout;
import com.open.ui.widget.dialog.TVUIDialog;
import com.open.ui.widget.dialog.TVUIDialogBuilder;
import com.open.ui.widget.dialog.TVUIDialogButtonAction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class DialogTest {

    Dialog mDialog;
    TVUIDialogBuilder mTVUIDialogBuilder;

    @Before
    public void setup() throws Exception {
        mTVUIDialogBuilder = new TVUIDialog.MessageDialogBuilder(InstrumentationRegistry.getContext())
                .setTitle("标题测试")
                .setMessage("清除配对后，您的遥控器需要重新进行配对")
                .addButtonAction("确定", new TVUIDialogButtonAction.OnCallBackListener() {
                    @Override
                    public void onCallBack(TVUIDialog dialog, View view) {
                        dialog.dismiss();
                    }
                })
                .addButtonAction("取消配对", new TVUIDialogButtonAction.OnCallBackListener() {
                    @Override
                    public void onCallBack(TVUIDialog dialog, View view) {
                        dialog.dismiss();
                    }
                });
//        .create();
    }

    @Test
    public void testShowDialog() {
//        mDialog.show();
//        mTVUIDialogBuilder.create().show();
        new TVUILinearLayout(InstrumentationRegistry.getContext());
//        new TVUICommonItemView(InstrumentationRegistry.getContext());
    }

}
